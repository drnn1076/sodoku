/* program to solve sodokus 
using backtracing with recursion 
in a simple brute-force tryouts

based on: https://www.baeldung.com/java-sudoku

Implementation in processing
Ricardo Cedeño Montaña
Universidad de Antioquia
Facultad de Comunicaciones
CC-SA-BY 2020

Last update 9.4.2020
*/

import java.util.Arrays;//import library to extend the array class

/* sodoku base
int[][] sodoku ={ 
{0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0},
};
*/

//int[][] sodoku = {
//{5,3,0,0,7,0,0,0,0},
//{6,0,0,1,9,5,0,0,0},
//{0,9,8,0,0,0,0,6,0},
//{8,0,0,0,6,0,0,0,3},
//{4,0,0,8,0,3,0,0,1},
//{7,0,0,0,2,0,0,0,6},
//{0,6,0,0,0,0,2,8,0},
//{0,0,0,4,1,9,0,0,5},
//{0,0,0,0,8,0,0,7,9},
//};

//int[][] sodoku = {
//{8,0,0,0,0,0,0,0,0},
//{0,0,3,6,0,0,0,0,0},
//{0,7,0,0,9,0,2,0,0},
//{0,5,0,0,0,7,0,0,0},
//{0,0,0,0,4,5,7,0,0},
//{0,0,0,1,0,0,0,3,0},
//{0,0,1,0,0,0,0,6,8},
//{0,0,8,5,0,0,0,1,0},
//{0,9,0,0,0,0,4,0,0} 
//};

//int[][] sodoku ={
//{3,7,0,0,0,6,0,0,0},
//{0,8,0,0,2,0,0,0,1},
//{0,0,0,1,7,0,8,0,0},
//{0,0,0,6,0,9,5,0,2},
//{0,1,0,0,0,0,0,6,0},
//{9,0,6,3,0,7,0,0,0},
//{0,0,7,0,9,2,0,0,0},
//{1,0,0,0,6,0,0,8,0},
//{0,0,0,4,0,0,0,2,5},
//};

//int[][] sodoku ={
//{6,2,0,0,0,0,0,0,0},
//{0,0,9,0,3,0,7,0,0},
//{0,0,3,0,0,0,0,1,2},
//{0,4,2,6,0,0,0,8,0},
//{0,0,0,3,0,8,0,0,0},
//{0,1,0,0,0,9,3,7,0},
//{5,6,0,0,0,0,9,0,0},
//{0,0,4,0,7,0,8,0,0},
//{0,0,0,0,0,0,0,3,5},
//};

int[][] sodoku ={
{1,0,0,0,0,0,0,0,0},
{0,6,8,0,0,0,0,0,9},
{0,5,0,8,0,0,3,2,0},
{0,0,0,0,8,3,0,0,6},
{0,2,0,0,1,0,0,5,0},
{8,0,0,4,6,0,0,0,0},
{0,8,9,0,0,1,0,4,0},
{4,0,0,0,0,0,5,9,0},
{0,0,0,0,0,0,0,0,3},
};

// initial variables
int boardSize = 9;
int boardStartIndex = 0;
int blockSize = 3;
int noValue = 0;
int minValue = 1;
int maxValue = 9;

void setup(){
  size(640,640);
  background(255);
  int cellSize = width/boardSize;
  
  //draw a 9x9 grid
  grid(cellSize);
  
  //function to solve
  solve(sodoku); //the argument is the sodoku 
  //fill the grid with the numbers of the problem
  for(int i=boardStartIndex; i<boardSize; i++){
    for (int j=boardStartIndex; j<boardSize; j++){
      fill(0);
      textSize(48);
      if(sodoku[j][i]==noValue){//leave the blanks
        text(" ", cellSize/blockSize + (cellSize*i), 
        cellSize/1.5 +(cellSize*j));
      }else{
        text(sodoku[j][i], cellSize/blockSize + (cellSize*i), 
        cellSize/1.5 +(cellSize*j));
      }   
    }
  }
  
}


//solve function receives the sodoku as an 2D array
boolean solve(int [][] sodoku){
  
  for(int j=boardStartIndex; j<boardSize; j++){ // rows
    for(int i =boardStartIndex;i<boardSize; i++){ //column
      if (sodoku[j][i]==noValue){//check if the cell has a zero
         for (int k = minValue; k<=maxValue; k++){//check for all numbers
            //println(i,j,k);
            if(possible(j,i,k)){//check if the number k is valid
              sodoku[j][i] = k;//if so added to the sodoku
              solve(sodoku);//call again the solve function with the new sodoku
               
            }
            
            sodoku[j][i] = noValue; // backtrace to the last position
            
          }
          return false;//there are still cells with zeros
         //sodoku[j][i] = 0;
        }
      } 
    }
    //if there are no zeros and all positions are valid 
    println(Arrays.deepToString(sodoku).replace("], ", "]\n"));
    printAnswer(sodoku);
    return true;
}


//check if a number is possible in a particular cell
boolean possible(int y, int x, int n){
  //check the rows
  for (int i = 0; i<9; i++){
    //println(sodoku[y][i]);
    if (sodoku[y][i] == n){
      return false;//the number is not possible in that cell
    }
  }
  //check the cols 
  for (int j = 0; j<9; j++){
    //println(sodoku[j][x], j);
    if (sodoku[j][x] == n){
      return false;//the number is not possible in that cell
      }
  }
  
  //check the block of three by three
  int x0 = y/blockSize*blockSize; 
  int y0 = x/blockSize*blockSize;
  for (int k =0; k<blockSize; k++){
     for (int l =0; l<blockSize; l++){
       //println(sodoku[x0+k][y0+l]);
        if (sodoku[x0+k][y0+l] == n){
          return false;//the number is not possible in that cell
        }
      }
    }
    return true; // the number is possible for this cell
  }

//draw a 9x9 grid
void grid(int cSize){
  for(int n=1; n<9; n++){
    if(n%blockSize==0){
      strokeWeight(4);
    }else{
      strokeWeight(1);
    }
    line(cSize*n,0,cSize*n,height);
    line(0,cSize*n,width,cSize*n);
    }
}

//print answers in red
void printAnswer(int[][] sodoku){  
  int cellSize = width/boardSize;
  for(int i=boardStartIndex; i<boardSize; i++){
    for (int j=boardStartIndex; j<boardSize; j++){
      fill(255,0,0);
      textSize(48);
        text(sodoku[j][i], cellSize/blockSize + (cellSize*i), 
        cellSize/1.5 +(cellSize*j));
      }   
    }
  }